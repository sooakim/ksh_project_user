package kr.edcan.ksh_project.Utils;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kimok_000 on 2016-01-15.
 */
public interface PaymentService {
    @POST("/purchase")
    @FormUrlEncoded
    void userPurchase(@Field("user_id") String user_id,
                                @Field("user_login_session") String user_login_session,
                                @Field("user_security_session") String user_security_session,
                                @Field("shop_nfc_tag") String shop_nfc_tag);
}
