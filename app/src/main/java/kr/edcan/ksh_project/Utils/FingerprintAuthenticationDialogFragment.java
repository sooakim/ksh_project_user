package kr.edcan.ksh_project.Utils;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import kr.edcan.ksh_project.Activity.MainActivity;
import kr.edcan.ksh_project.R;
import kr.edcan.ksh_project.UiHelpers.FingerprintUiHelper;

/**
 * Created by kimok_000 on 2016-01-12.
 */
public class FingerprintAuthenticationDialogFragment extends DialogFragment implements TextView.OnEditorActionListener, FingerprintUiHelper.Callback {
    private Button mCancelButton;
    private Button mSecondDialogButton;
    private View mFingerprintContent;
    private View mBackupContent;
    private EditText mPassword;
    private CheckBox mUserFingerprintFutureCheckBox;
    private TextView mPasswordDescriptionTextView;
    private TextView mNewFingerprintEnrolledTextView;

    private Stage mStage = Stage.FINGERPRINT;

    private FingerprintManager.CryptoObject mCryptoObject;
    private FingerprintUiHelper mFingerprintUiHelper;
    private MainActivity mActivity;

    @Inject
    FingerprintUiHelper.FingerprintUiHelperBuilder mFingerprintUiHelperBuilder;
    @Inject
    InputMethodManager mInputMethodManager;
    @Inject
    SharedPreferences mSharedPreferences;

    @Inject
    public FingerprintAuthenticationDialogFragment() {
    } //익명 생성자

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //화면 방향이 바뀌어서 액티비티가 재생성된 경우에는 새 Fragment를 생성하지 마시오.
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.sign_in));
        View v = inflater.inflate(R.layout.fingerprint_dialog_container, container, false);
        mCancelButton = (Button) v.findViewById(R.id.cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mSecondDialogButton = (Button) v.findViewById(R.id.second_dialog_button);
        mSecondDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mStage == Stage.FINGERPRINT) {
                    goToBackup();
                } else {
                    verifyPassword();
                }
            }
        });

        mFingerprintContent = v.findViewById(R.id.fingerprint_container);
        mBackupContent = v.findViewById(R.id.backup_container);
        mPassword = (EditText) v.findViewById(R.id.password);
        mPassword.setOnEditorActionListener(this);
        mPasswordDescriptionTextView = (TextView) v.findViewById(R.id.password_description);
        mUserFingerprintFutureCheckBox = (CheckBox) v.findViewById(R.id.use_fingerprint_in_future_check);
        mNewFingerprintEnrolledTextView = (TextView) v.findViewById(R.id.new_fingerprint_enrolled_description);
        mFingerprintUiHelper = mFingerprintUiHelperBuilder.build(
                (ImageView) v.findViewById(R.id.fingerprint_icon),
                (TextView) v.findViewById(R.id.fingerprint_status), this);
        updateStage();

        //지문 인식이 불가능한 경우, 즉시 백업 인증으로 전환
        // 비밀번호 화면
        if (!mFingerprintUiHelper.isFingerprintAuthAvailable()) {
            goToBackup();   //지문인식이 불가능하면 백업으로
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mStage == Stage.FINGERPRINT) {
            mFingerprintUiHelper.startListening(mCryptoObject);
        }
    }

    public void setStage(Stage stage) {
        mStage = stage;
    }

    @Override
    public void onPause() {
        super.onPause();
        mFingerprintUiHelper.stopListening();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
    }

    //지문으로 인증할 때 암호 오브젝트를 통과하도록 설정

    public void setCryptoObject(FingerprintManager.CryptoObject cryptoObject) {
        mCryptoObject = cryptoObject;
    }

    //백업 비밀번호 화면으로 전환. 지문인식이 불가능하거나 사용자가
    //비밀번호 인증 버튼을 눌러 선택한 경우에 발생.
    //사용자가 지문 시도를 너무 많애 했을 때에도 발생할 수 있음.

    private void goToBackup() {
        mStage = Stage.PASSWORD;
        updateStage();
        mPassword.requestFocus();

        //키보드 보이기
        mPassword.postDelayed(mShowKeyBoardRunnable, 500);

        //지문 인식이 더 이상 사용되지 않음. 지문인식 종료
        mFingerprintUiHelper.stopListening();
    }

    //현재 입력한 비밀번호가 일치하여 다이얼로그가 종료된 경우 액티비티에 결과 반환

    public void verifyPassword() {
        if (!checkPassword(mPassword.getText().toString())) {
            return; //패스워드 불일치
        }
        if (mStage == Stage.NEW_FINGERPRINT_ENROLLED) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putBoolean(getString(R.string.use_fingerprint_to_authenticate_key),
                    mUserFingerprintFutureCheckBox.isChecked());
            editor.apply();

            if (mUserFingerprintFutureCheckBox.isChecked()) {
                // Re-create the key so that fingerprints including new ones are validated.
                mActivity.createKey();
                mStage = Stage.FINGERPRINT;
            }
        }

        mPassword.setText("");
        mActivity.onPurchased(false);   //지문인식 없이
        dismiss();
    }

    //패스워드가 일치하면 true 반환, 아니면 false
    private boolean checkPassword(String password) {
        //암호가 항상 일치한다고 가정
        //현실에서는 서버에서 승인해야됨
        return password.length() > 0;
    }

    private final Runnable mShowKeyBoardRunnable = new Runnable() {
        @Override
        public void run() {
            mInputMethodManager.showSoftInput(mPassword, 0);
        }
    };

    private void updateStage() {
        switch (mStage) {
            case FINGERPRINT: { //지문일 경우
                mCancelButton.setText(R.string.cancel);
                mSecondDialogButton.setText(R.string.use_password);
                mFingerprintContent.setVisibility(View.VISIBLE);
                mBackupContent.setVisibility(View.GONE);    //백업 비번 버튼 숨기기
                break;
            }

            case NEW_FINGERPRINT_ENROLLED: {   //새 지문 추가
                //내부에서 처리
                //TODO : 지문 설정 창으로 이동 하세요.
                break;
            }

            case PASSWORD: {   //백업 비밀번호
                mCancelButton.setText(R.string.cancel);
                mSecondDialogButton.setText(R.string.ok);
                mFingerprintContent.setVisibility(View.GONE);
                mBackupContent.setVisibility(View.VISIBLE);
                if (mStage == Stage.NEW_FINGERPRINT_ENROLLED) {   //새 지문이 추가된 경우
                    mPasswordDescriptionTextView.setVisibility(View.GONE);
                    mNewFingerprintEnrolledTextView.setVisibility(View.VISIBLE);
                    mUserFingerprintFutureCheckBox.setVisibility(View.VISIBLE);
                }
                break;
            }
        }
    }

    @Override
    public void onAuthenticated() {
        //FingerprintUiHelper로 부터의 콜백. 인증이 성공했다는 것을 액티비티에 알려주자.
        mActivity.onPurchased(true);    //지문인식시
        dismiss();
    }

    @Override
    public void onError() {
        goToBackup();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            verifyPassword();
            return true;
        }
        return false;
    }

    //----------------------------------------------------------------------------------------------------------------------------

    public enum Stage {
        FINGERPRINT,
        NEW_FINGERPRINT_ENROLLED,
        PASSWORD
    }
}
