package kr.edcan.ksh_project.Data;

/**
 * Created by kimok_000 on 2016-01-14.
 */
public class LogData {
    private String shop_name;   //샵 이름
    private String shop_address;    //샵 주소
    private String item_price;  //물건 가격
    private String item_price_type; //가격 단위 - 기본값 : 원화

    public LogData(String shop_name, String shop_address, String item_price, String item_price_type) {
        this.shop_name = shop_name;
        this.shop_address = shop_address;
        this.item_price = item_price;
        this.item_price_type = item_price_type;
    }

    public String getShop_name() {
        return shop_name;
    }

    public String getShop_address() {
        return shop_address;
    }

    public String getItem_price() {
        return item_price;
    }

    public String getItem_price_type() {
        return item_price_type;
    }
}
