package kr.edcan.ksh_project.Data;

import com.orm.SugarRecord;

/**
 * Created by kimok_000 on 2016-01-15.
 */
public class Setting extends SugarRecord {
    private String setting_title;
    private String setting_content;
    private String setting_button;

    public Setting(){}

    public Setting(String setting_title, String setting_content, String setting_button) {
        this.setting_title = setting_title;
        this.setting_content = setting_content;
        this.setting_button = setting_button;
    }

    public String getSetting_title() {
        return setting_title;
    }

    public String getSetting_content() {
        return setting_content;
    }

    public String getSetting_button() {
        return setting_button;
    }
}
