package kr.edcan.ksh_project.UiHelpers;

import android.annotation.TargetApi;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.VisibleForTesting;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import kr.edcan.ksh_project.R;

/**
 * Created by kimok_000 on 2016-01-12.
 */
@TargetApi(Build.VERSION_CODES.M)   //안드로이드 6.0에서만 작동가능
public class FingerprintUiHelper extends FingerprintManager.AuthenticationCallback {
    @VisibleForTesting private static final long ERROR_TIMEOUT_MILLS = 1600;
    @VisibleForTesting private static final long SUCCES_DELAY_MILLS = 1300;

    private final FingerprintManager mFingerprintManager;
    private final ImageView mIcon;
    private final TextView mErrorTextView;
    private final Callback mCallback;
    private CancellationSignal mCancellationSignal;

    @VisibleForTesting private boolean mSelfCancelled;

    public static class FingerprintUiHelperBuilder {
        private final FingerprintManager mFingerprintManager;

        @Inject
        public FingerprintUiHelperBuilder(FingerprintManager mFingerprintManager) {
            this.mFingerprintManager = mFingerprintManager;
        }

        public FingerprintUiHelper build(
                ImageView icon,
                TextView errorTextView,
                Callback callback) {

            return new FingerprintUiHelper(mFingerprintManager, icon, errorTextView, callback);
        }
    }

    public FingerprintUiHelper(
            FingerprintManager fingerprintManager,
            ImageView mIcon,
            TextView mErrorTextView,
            Callback mCallback) {

        this.mFingerprintManager = fingerprintManager;
        this.mIcon = mIcon;
        this.mErrorTextView = mErrorTextView;
        this.mCallback = mCallback;
    }

    public boolean isFingerprintAuthAvailable() {   //지문 인식이 가능한지 체크
        return mFingerprintManager.isHardwareDetected()     //지원되는 하드웨어?
                && mFingerprintManager.hasEnrolledFingerprints();   //지문이 등록이 되어있나?
    }

    public void startListening(FingerprintManager.CryptoObject cryptoObject) {
        if (!isFingerprintAuthAvailable()) {
            return; //지문인식이 불가능한 경우 중단
        }

        mCancellationSignal = new CancellationSignal();
        mSelfCancelled = false;
        mFingerprintManager.authenticate(
                cryptoObject,
                mCancellationSignal, 0, this, null);
        //mIcon.setImageResource();//지문인식 이미지 아이콘
    }

    public void stopListening() {    //정지 신호 처리
        if (mCancellationSignal != null) {    //취소 신로가 있을 떄
            mSelfCancelled = true;  //자가 취소 true
            mCancellationSignal.cancel();   //취소
            mCancellationSignal = null; //취소 신호 없음으로 초기화
        }
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        if (!mSelfCancelled) {    //자가 정지 상태가 아님
            showError(errString);
            mIcon.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mCallback.onError();
                }
            }, ERROR_TIMEOUT_MILLS);
        }
    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        showError(helpString);
    }

    @Override
    public void onAuthenticationFailed() {  //인식 실패
        super.onAuthenticationFailed();
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        mErrorTextView.removeCallbacks(mResetErrorTextRunnable);
        mIcon.setImageResource(R.drawable.ic_fingerprint_success);
        mErrorTextView.setTextColor(
                mErrorTextView.getResources().getColor(R.color.success_color, null));
        mErrorTextView.setText(
                mErrorTextView.getResources().getString(R.string.fingerprint_success));
        mIcon.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCallback.onAuthenticated();
            }
        }, SUCCES_DELAY_MILLS);
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------

    private void showError(CharSequence error) {
        mIcon.setImageResource(R.drawable.ic_fingerprint_error);
        mErrorTextView.setText(error);
        mErrorTextView.setTextColor(
                mErrorTextView.getResources().getColor(R.color.warning_color, null));
        mErrorTextView.removeCallbacks(mResetErrorTextRunnable);
        mErrorTextView.postDelayed(mResetErrorTextRunnable, ERROR_TIMEOUT_MILLS);
    }

    Runnable mResetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {
            mErrorTextView.setTextColor(
                    mErrorTextView.getResources().getColor(R.color.hint_color, null));
            mErrorTextView.setText(
                    mErrorTextView.getResources().getString(R.string.fingerprint_hint));
            mIcon.setImageResource(R.drawable.ic_fp_40px);
        }
    };

    public interface Callback {
        void onAuthenticated();

        void onError();
    }
}