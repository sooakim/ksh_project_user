package kr.edcan.ksh_project.Activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.inject.Inject;

import kr.edcan.ksh_project.InjectedApplication;
import kr.edcan.ksh_project.R;
import kr.edcan.ksh_project.Utils.FingerprintAuthenticationDialogFragment;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String DIALOG_FRAGMENT_TAG = "myFragment";
    private static final String SECRET_MESSAGE = "Very secrest message";

    private static final String KEY_NAME = "my_key";

    @Inject
    KeyguardManager mKeyguardManager;
    @Inject
    FingerprintManager mFingerprintManager;
    @Inject
    FingerprintAuthenticationDialogFragment mFragment;
    @Inject
    KeyStore mKeyStore;
    @Inject
    KeyGenerator mKeyGenerator;
    @Inject
    Cipher mCipher;
    @Inject
    SharedPreferences mSharedPreferences;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((InjectedApplication) getApplication()).inject(this);

        setContentView(R.layout.activity_main);
        Button purchaseButton = (Button) findViewById(R.id.purchase_button);
        if (!mKeyguardManager.isKeyguardSecure()) {
            //사용자가 지문이나 화면 잠금을 설정하지 않았을 때에 메시지를 보여줌
            Toast.makeText(getApplicationContext(),
                    "보안 잠금 화면이 설정되어 있지 않습니다.\n"
                            + "'설정 -> 보안 -> 지문'으로 가서 지문을 설정하세요.",
                    Toast.LENGTH_LONG).show();
            purchaseButton.setEnabled(false);
            return;
        }

        //noinspection ResourceType
        if (!mFingerprintManager.hasEnrolledFingerprints()) {
            purchaseButton.setEnabled(false);
            //지문이 등록되어 있지 않을 때 발생
            Toast.makeText(getApplicationContext(),
                    "'설정 -> 보안 -> 지문'에 가서 최소 한개의 지문을 등록하세요.'",
                    Toast.LENGTH_LONG).show();
            return;
        }

        //앞의 조건을 모두 만족할 때 발생
        createKey();
        purchaseButton.setEnabled(true);
        purchaseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.confirmation_message).setVisibility(View.GONE);
                findViewById(R.id.encrypted_message).setVisibility(View.GONE);

                //나중을 위해 암호화 오브젝트를 설정합니다. 이 오브젝트는 지문을 사용하여 인증됩니다,
                if (initCipher()) {
                    //지문 다이얼로그를 보여줍니다. 유저는 암호화된 지문 사용 또는 서버-사이드 비밀번호 인증이라는 옵션을 가집니다.
                    mFragment.setCryptoObject(new FingerprintManager.CryptoObject(mCipher));
                    boolean useFingerprintPreference = mSharedPreferences
                            .getBoolean("use_fingerprint_to_authenticate_key",
                                    true);
                    if (useFingerprintPreference) {
                        mFragment.setStage(
                                FingerprintAuthenticationDialogFragment.Stage.FINGERPRINT);
                    } else {
                        mFragment.setStage(
                                FingerprintAuthenticationDialogFragment.Stage.PASSWORD);
                    }
                    mFragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
                } else {
                    //잠금 화면이 비활성화되거나 지문이 등록되었을 떼 발생.
                    //유저가 나중에 지문을 인증하고 싶어할 때 먼저 비밀번호를 인증하기 위해 다이얼로그를 보여줌.
                    mFragment.setCryptoObject(new FingerprintManager.CryptoObject(mCipher));
                    mFragment.setStage(
                            FingerprintAuthenticationDialogFragment.Stage.NEW_FINGERPRINT_ENROLLED);
                    mFragment.show(getFragmentManager(), DIALOG_FRAGMENT_TAG);
                }

            }
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean initCipher() {
        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(KEY_NAME, null);
            mCipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    public void onPurchased(boolean withFingerprint) {
        if (withFingerprint) {
            //유저가 지문으로 인증하면 암호화그래피를 사용해 승인하고 완료 메시지를 보여줌.
            tryEncrypt();
        } else {
            //백업 비밀번호로 인증을 하면 그냥 완료 메시지를 보여줌
            showConfirmation(null);
        }
    }

    //지문이 암호화 정보를 보여주는데 사용되었을 떼 보여줌
    private void showConfirmation(byte[] encryted) {
        findViewById(R.id.confirmation_message).setVisibility(View.VISIBLE);
        if (encryted != null) {
            TextView v = (TextView) findViewById(R.id.encrypted_message);
            v.setVisibility(View.VISIBLE);
            v.setText(Base64.encodeToString(encryted, 0));
        }
    }

    private void tryEncrypt() {
        try {
            byte[] encryted = mCipher.doFinal(SECRET_MESSAGE.getBytes());
            showConfirmation(encryted);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            Toast.makeText(getApplicationContext(), "생성된 키로 데이터를 암호화하는데 실패했습니다."
                    + "결제를 재시도 하세요.", Toast.LENGTH_LONG).show();
            Log.e(TAG, "Failed to encrypt the data with the generated key." + e.getMessage());
        }

    }

    //사용자가 지문으로 인증을 시도한 뒤에만 사용될 수 있는 안드로이드 키 스토어의 대칭키를 생성
    @TargetApi(Build.VERSION_CODES.M)
    public void createKey() {
        try {
            mKeyStore.load(null);
            mKeyGenerator.init(new KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                            //사용자가 모든 키를 사용하기 위해 지문으로 인증을 요청
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            mKeyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
