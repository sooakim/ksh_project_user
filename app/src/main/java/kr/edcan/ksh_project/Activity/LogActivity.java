package kr.edcan.ksh_project.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

import kr.edcan.ksh_project.Adapter.LogDataAdapter;
import kr.edcan.ksh_project.Data.LogData;
import kr.edcan.ksh_project.R;

/**
 * Created by kimok_000 on 2016-01-15.
 */
public class LogActivity extends AppCompatActivity {
    private ArrayList<LogData> arrayList;
    private LogDataAdapter adapter;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        setDefault();
    }

    private void setDefault() {
        listView = (ListView) findViewById(R.id.listview);
        arrayList = new ArrayList<>();
        arrayList.add(new LogData("일레븐세븐 선린점","용산구 청파로 구창3가 선린인터넷고등학교","4700","원"));
        arrayList.add(new LogData("PS25 선린점", "용산구 청파로 준석4가 PS", "2100", "원"));

        setListView();
    }

    private void setListView() {
        adapter = new LogDataAdapter(getApplicationContext(), arrayList);
        listView.setAdapter(adapter);
    }
}
