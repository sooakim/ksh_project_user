package kr.edcan.ksh_project.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.awt.font.TextAttribute;
import java.io.PushbackReader;

import kr.edcan.ksh_project.R;

/**
 * Created by Sunrin on 2016-01-15.
 */
public class PayModeActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView button_charge, button_purchase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymode);

        setDefault();
    }

    private void setDefault() {
        button_charge = (TextView) findViewById(R.id.charge);
        button_purchase = (TextView) findViewById(R.id.purchase);
        button_charge.setOnClickListener(this);
        button_purchase.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.charge : {
                Intent goCharge = new Intent(getApplicationContext(), ChargeActivity.class);
                goCharge.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(goCharge);
                break;
            }

            case R.id.purchase : {
                Intent goPurchase = new Intent(getApplicationContext(), PayActivity.class);
                goPurchase.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(goPurchase);
                break;
            }
        }
    }
}
