package kr.edcan.ksh_project.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import kr.edcan.ksh_project.Adapter.SettingDataAdapter;
import kr.edcan.ksh_project.Data.SettingData;
import kr.edcan.ksh_project.R;

/**
 * Created by kimok_000 on 2016-01-14.
 */
public class TempActivity extends AppCompatActivity implements View.OnClickListener {
    private FloatingActionButton fab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        setDefault();
    }

    private void setDefault() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab: {
                Intent goPayMode = new Intent(getApplicationContext(), PayModeActivity.class);
                goPayMode.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(goPayMode);
                break;
            }
        }
    }
}
