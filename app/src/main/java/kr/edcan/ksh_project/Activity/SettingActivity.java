package kr.edcan.ksh_project.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceFragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

import kr.edcan.ksh_project.Adapter.SettingDataAdapter;
import kr.edcan.ksh_project.Data.SettingData;
import kr.edcan.ksh_project.R;

/**
 * Created by Sunrin on 2016-01-13.
 */
public class SettingActivity extends AppCompatActivity {
    private ArrayList<SettingData> arrayList;
    private SettingDataAdapter adapter;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        setDefault();
    }

    private void setDefault() {
        listView = (ListView) findViewById(R.id.listview);
        arrayList = new ArrayList<>();
        arrayList.add(new SettingData("이름", "김수한", "변경"));
        arrayList.add(new SettingData("비밀번호", "**********", "변경"));
        arrayList.add(new SettingData("월 결제한도", "300,000", "변경"));
        arrayList.add(new SettingData("계정 상태", "결제중(일시적 사용 불가)", "분실 신고"));

        setListView();
    }

    private void setListView() {
        adapter = new SettingDataAdapter(getApplicationContext(), arrayList);
        listView.setAdapter(adapter);
    }
}
