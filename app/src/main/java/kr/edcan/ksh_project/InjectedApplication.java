package kr.edcan.ksh_project;

import android.app.Application;
import android.util.Log;

import dagger.ObjectGraph;
import kr.edcan.ksh_project.Utils.FingerprintModule;

/**
 * Created by Sunrin on 2016-01-13.
 */
public class InjectedApplication extends Application{
    private static final String TAG = InjectedApplication.class.getSimpleName();
    private ObjectGraph mObjectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        initObjectGraph(new FingerprintModule(this));
    }

    public void initObjectGraph(Object module){
        mObjectGraph = module != null ? ObjectGraph.create(module) : null;
    }

    public void inject(Object object){
        if(mObjectGraph == null){
            Log.i(TAG, "Object graph is not initialized.");
            return;
        }
        mObjectGraph.inject(object);
    }
}
