package kr.edcan.ksh_project.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.ksh_project.Data.LogData;
import kr.edcan.ksh_project.R;

/**
 * Created by kimok_000 on 2016-01-14.
 */
public class LogDataAdapter extends ArrayAdapter<LogData> {
    private LayoutInflater mInflater;
    private Context context;

    public LogDataAdapter(Context context, ArrayList<LogData> object) {
        super(context, 0, object);
        this.context = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        View view = null;
        // 현재 리스트의 하나의 항목에 보일 컨트롤 얻기
        if (v == null) {
            // XML 레이아웃을 직접 읽어서 리스트뷰에 넣음
            view = mInflater.inflate(R.layout.listview_log_content, null);
        } else {
            view = v;
        }
        final LogData data = this.getItem(position);
        if (data != null) {
            //화면 출력
            TextView shop_name = (TextView) view.findViewById(R.id.shop_name);
            TextView shop_address = (TextView) view.findViewById(R.id.shop_address);
            TextView item_price = (TextView) view.findViewById(R.id.item_price);
            TextView item_price_type = (TextView) view.findViewById(R.id.item_price_type);

            shop_name.setText(data.getShop_name());
            shop_address.setText(data.getShop_address());
            item_price.setText("- "+data.getItem_price());
            item_price_type.setText(data.getItem_price_type());
        }
        return view;
    }
}
